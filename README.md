## "Robot-induced hallucinations in Parkinson’s disease depend on altered sensorimotor processing in fronto-temporal network"
Bernasconi, Blondiaux et al., 2021 ; 
DOI: 10.1126/scitranslmed.abc8362 ;
fosco.bernasconi@gmail.com 


# Study 1
### Study 1.1 - riPH inpatients with PD (asynchronous versus synchronous stimulation)

Code and data for "Asynchronous versus synchronous stimulation"

- ./Clinical: R script(s) to assess differences between clinical populations & behavioral data *.csv
- ./Questionnaire: R script(s) to analyse questionnaires & data *.csv
- ./Laterality: R script(s) to analyse side of the reported robot-induced PH & behavioral data *.csv

### Study 1.2 - riPH inpatients with PD (sensorimotor delay dependency)  

Code and data for "Sensorimotor delay dependency"

- ./Movement: R script to analyse the movement pattern performed with the robotic device & behavioral data *.csv
- ./Behavior : R script to analyse the behavioral response during the robotic task & behavioral data *.csv


# Study 2

### Study 2.1 - Brain mechanisms of riPH in healthy participants using MR-compatible robotics  

Code and data for "Brain mechanisms of riPH in healthy participants using MR-compatible robotics"

- ./Behavior: R script(s) to analyse questionnaires for moc scanner and fMRI study  & behavioral data *.csv
- ./fMRI : matlab routines for preprocessing & statistical analysis 

### Study 2.2 - cPH-network for sPH and riPH   

Code and data for "Common PH-network for sPH and riPH"

- ./fMRI : matlab routines for preprocessing & statistical analysis

fMRI data can be found here: https://zenodo.org/record/4423384#.ZDe4Rs5BxmO

# Study 3

### Functional connectivity analysis in cPH-network in patients with PD & Functional disconnection within the cPH-network and cognitive decline in patients with PD and PH

Code and data for "Functional connectivity analysis in cPH-network in patients with PD"

- ./Clinical: R script(s) to assess differences between clinical populations & behavioral data *.csv
- ./fMRI_PH_network :  matlab routines for functinal connectivity in the cPH-network & behavioral data *.csv
- ./Statististics: R script(s) to conduct decoding, correlations and statistical analysis & behavioral data *.csv
