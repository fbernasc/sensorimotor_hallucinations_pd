%% First level subject pre-processing and denoising steps
% Standard CONN toolbox pipeline is used
%CONN toolbox: https://web.conn-toolbox.org/
%Parkinsonian patients N=32
%ROIs: 6 PH-network, 6 control clusters

% Adapted and written by Giedre Stripeikyte

%% Load paths and data

%Add SPM path
addpath '/home/stripeik/matlab/spm/spm12'
%Add CONN toolbox path
addpath(genpath('/home/stripeik/matlab/CONN/conn'))

%Load subject folders
path_subjects='/EXTDATAVOL/fbernasc/restings_Pago_Filezilla/';
folders=dir(path_subjects);
folders={folders.name}';
folders = setdiff(folders,{'.';'..'});

%PH-network and control clusters
ROIpath = '/EXTDATAVOL/stripeik/PD_rsfmri/ROI'; 
ROIs    = spm_select('List',ROIpath,'.nii');

%% SETUP

%Define batch properties
clear BATCH;
BATCH.filename = ['/EXTDATAVOL/stripeik/PD_rsfmri/conn_PD_PH'];

BATCH.Setup.isnew=1;
BATCH.Setup.RT = 2;
BATCH.Setup.nsubjects = 32;    %PAY ATTENTION! The number of subjects to be processed
BATCH.Setup.acquisitiontype = 1; % continous (default value)
BATCH.Setup.analyses = [1,2,3]; % ROI to ROI and seed to voxel
BATCH.Setup.voxelmask = 1; %1-fixed template mask; 2-implicit uses subject specific mask
BATCH.Setup.voxelresolution = 3; % functional space used
BATCH.Setup.outputfiles = [0,0,1,1,1]; % write nii volumes for r-maps, p-maps and FDR-p-maps 

% specify targeted ROIs
BATCH.Setup.rois.names = cellstr(ROIs);
for h=1:size(ROIs,1)
    BATCH.Setup.rois.files{h}{1} = fullfile(ROIpath,ROIs(h,:)); 
    BATCH.Setup.rois.mask(h) = [1]; %1 mask/0 not mask each ROI with Gray matter voxels 
end
BATCH.Setup.rois.dimensions = repmat({1},1,length(cellstr(ROIs)));%takes the mean times series for each ROIs 
 
for i = 1:length(folders)
    Nifti_outputdir = [path_subjects,folders{i},filesep];
    Nifti_outputdir_anat = [path_subjects,folders{i},filesep,'anat',filesep];
    Nifti_outputdir_func = [path_subjects,folders{i},filesep,'func',filesep];
    t1_file{i} = spm_select('FPList',Nifti_outputdir_anat,'wcmprage');
    c1_file{i} = spm_select('FPList',Nifti_outputdir_anat,'^wc1');
    c2_file{i} = spm_select('FPList',Nifti_outputdir_anat,'^wc2');
    c3_file{i} = spm_select('FPList',Nifti_outputdir_anat,'^wc3');
% specificy functional data per subject {nsub}{nsess}  
    BATCH.Setup.functionals{i}{1} = spm_select('FPlist',Nifti_outputdir_func,'sw.*\.nii$');
% specificy structural data per subject
    BATCH.Setup.structurals{i} = t1_file{i};
    BATCH.Setup.masks.Grey.files{i} = c1_file{i};
    BATCH.Setup.masks.Grey.dimensions=1;
    BATCH.Setup.masks.White.files{i} = c2_file{i};
    BATCH.Setup.masks.White.dimensions=3;
    BATCH.Setup.masks.CSF.files{i} = c3_file{i};
    BATCH.Setup.masks.CSF.dimensions=3; 
end

% specify conditions per subject {ncond}{nsub}{nsess}
BATCH.Setup.conditions.names = {'rest'};
for i = 1:length(folders)
    Nifti_outputdir = [path_subjects,folders{i},filesep];
    BATCH.Setup.conditions.onsets{1}{i}{1} = [0];
    BATCH.Setup.conditions.durations{1}{i}{1} = [inf];
    BATCH.Setup.covariates.names = {'motion'};
    BATCH.Setup.covariates.files{1}{i}{1} = spm_select('FPList',Nifti_outputdir_func,'rp.*\.txt$');% mvt
    BATCH.Setup.done = 1; % run the SETUP step
end

%% PREPROCESSING
% 
BATCH.Preprocessing.filter = [0.01, 0.1]; % set the passband filter limits.
BATCH.Preprocessing.confounds.names = {'White Matter', 'CSF', 'motion','Effect of rest'};
BATCH.Preprocessing.confounds.dimensions = {'3','3','6','1'}; %changed dimensions from 1 to 3
BATCH.Preprocessing.confounds.deriv = {'0','0','1','1'};
BATCH.Preprocessing.done = 1; % run the PREPROCESSING step
% 
%% FIRST-LEVEL ANALYSIS
BATCH.Analysis.analysis_number = 1;
BATCH.Analysis.type = 3; % do ROI to ROI and seed to voxel analysis
BATCH.Analysis.measure = 1; % measure bivariate correlation
BATCH.Analysis.weight = 1; % use hrf as weight
BATCH.Analysis.sources.names = cellstr(ROIs);
BATCH.Analysis.sources.dimensions = repmat({1},1,length(cellstr(ROIs)));
BATCH.Analysis.sources.deriv = repmat({0},1,length(cellstr(ROIs)));
BATCH.Analysis.done = 1; % run the ANALYSIS step 

%% run the BATCH
conn_batch(BATCH);

