%% Check framewise displacement
% Giedre Stripeikyte
% 2019
%%
clear all;clc;
%% Data:
path_subjects='/EXTDATAVOL/fbernasc/restings_Pago_Filezilla/';
folders=dir(path_subjects);
folders={folders.name}';
folders=setdiff(folders,{'.';'..'});

for i=1:length(folders)
    fprintf('Processing subject %i\n',i);
    path_s=[path_subjects,folders{i},filesep,'func',filesep];
    result_path= path_s;
    %% Calculates the framewise displacement:
    [FD]=Calculate_FD(result_path,i,0.5);
    FD_S(i,:)=FD;
end

save('/EXTDATAVOL/stripeik/PD_rsfmri/FD_PD.mat', 'FD_S')