% Calculates the framewise displacement using the
% bramila_framewiseDisplacement function beased on Power paper.
function [FD]=Calculate_FD(result_path,subject,threshold)
%% Calculates the FD for the rest:
filename=find_file(result_path,'rp_');
cfg.motionparam=filename;
cfg.prepro_suite = 'spm';
cfg.radius =50;
[fwd,rms]=bramila_framewiseDisplacement(cfg);
outofthreshold=length(find(fwd>threshold));
if sum(outofthreshold)~=0
    fprintf('S%s Movement above threshold: %i points above in rest\n',subject,outofthreshold);
end
% Calculate the mean FD:
FD = mean(fwd);
fprintf('\nThe mean FD of Rest is %f\n',FD)

end