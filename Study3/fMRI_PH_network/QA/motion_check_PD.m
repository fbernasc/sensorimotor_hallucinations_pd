%% Check motion of .nii images; translation
%Giedre Stripeikyte

clear all
addpath '/home/stripeik/matlab/spm/spm12'
path = ('/EXTDATAVOL/fbernasc/restings_Pago_Filezilla/');
folders=dir(path);
folders={folders.name}';
folders=setdiff(folders,{'.';'..'});

%Treshold to consider as excessive movement. 2 mm robust, 4 mm liberal.
%Here checking only z axis of translation
threshold = 2;
for i = 1:32
    folder_rest=[path,folders{i},filesep,'func',filesep];
    fprintf('Processing subject %s ..\n',num2str(i))
    %% SELECT relevant files
    rpRest = spm_load(spm_select('FPList', folder_rest,'rp_.*\.txt$'));

 
    %% PLOT
    save_path = ('/EXTDATAVOL/stripeik/PD_rsfmri/QA_mov/');
    Restfig = figure('visible','off');
    %Rest
    subplot(2,1,1);plot(rpRest(:,1:3));
    set(gca,'xlim',[0 size(rpRest,1)+1]);
    title(['Subject' num2str(i) ' Rest: Translation']);
    ylabel('mm')
    xlabel('scans')
    legend ('x','y','z');
    subplot(2,1,2);plot(rpRest(:,4:6));
    set(gca,'xlim',[0 size(rpRest,1)+1]);
    title(['Subject' num2str(i) ' Rest: Rotation']);
    ylabel('degrees')
    xlabel('scans')
    legend('pitch','roll','yaw'); 
    saveas (Restfig, [save_path,'Subject' num2str(i) '_Rest']);
    saveas (Restfig, [save_path,'Subject' num2str(i) '_Rest.png']);
     
    %% CHECK MOVEMENT
      %Rest
    count4 = 0;
    mvRest = [];
    fprintf('\n checking movement Rest for Subject %s', num2str(i));
    for R = 1:length(rpRest) 
        
        if R >= 2
            mv = rpRest(R,3) - rpRest(R-1,3);
            if abs(mv) >= threshold 
                fprintf('\n Subject %s z translation movement Rest, scan %s', num2str(i),num2str(R)); 
                count4 = count4 + 1;
                mvRest(count4) = R; 
                save ([save_path 'Subject' num2str(i) '_mvRest'],'mvRest');
            end   
        end
    end
    if isempty(mvRest) 
       fprintf('\n NO MOVEMENT Rest for Subject %s', num2str(i)); 
    end
 
end

