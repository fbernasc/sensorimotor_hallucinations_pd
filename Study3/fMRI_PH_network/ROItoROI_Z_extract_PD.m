%% Extract rsfMRI ROI-to-ROI connectivity data, Normalised Z
%Used for Parkinsonian patients. 
%PH netwrok and control ROIs
%Adapt based on the number of ROIs

%Written by Giedre Stripeikyte

%% DATA
clear all
work_dir = pwd;

data_path = '/EXTDATAVOL/stripeik/PD_rsfmri/conn_PD_PH/results/firstlevel/SBC_01';

files=dir(data_path);
files={files.name}';
files_id = strfind(files,sprintf('resultsROI_Subject'));
files_con=files(~cellfun('isempty',files_id));
files_con=fullfile(data_path,filesep,files_con);

%Load demographic data: age and group
load('/EXTDATAVOL/stripeik/PD_rsfmri/Age_PD.mat')
load('/EXTDATAVOL/stripeik/PD_rsfmri/PD_PH.mat')

%% Connections
load([data_path filesep 'resultsROI_Condition001.mat'],'names');
Name = names;
Name = erase(Name, '.nii');
Name = erase(Name, '_');
Namecomb = combntns(Name,2);
for n=1:length(Namecomb)
    Connections{n}=[Namecomb{n,1} '-' Namecomb{n,2}];
end

Connections = repmat(Connections',length(files_con),1);

%How many possibe connetions
con=length(Namecomb);
Order = [1:con]'; 
Order = repmat(Order,length(files_con),1);

%% Long format of the data
for i = 1:length(files_con)
    load(files_con{i},'Z'); 

    %%%%%%%%%%%%%%%%%%%%%%%% N.B
    %ADAPT!
    %All connections connectivity
    Clusters = Z(1:12,1:12);
    A=Clusters(:);
    A=round(A,7);
    B=unique(A,'rows','stable');
    B=B(~isnan(B));
    if (length(B)) ~= con
        fprintf('Length is wrong for SUBJECT: %i \n',i)
        break
    end
    
    if PD_PH(i)==1
        group = 1;
        group_name = {'PH+'};
    else
        group = 0;
        group_name = {'PH-'};
    end
    %for long format
    if i ==1
        Group = repmat(group,con,1); 
        GroupName = repmat(group_name,con,1);
        Subject = repmat(i,con,1);
        AgeSubj = repmat(Age_PD(i),con,1);
        R=B;  
    elseif i>1 
        Group((length(Group)+1):(length(Group)+con),1) = group;
        GroupName((length(GroupName)+1):(length(GroupName)+con),1) = group_name;
        Subject((length(Subject)+1):(length(Subject)+con),1) = i;
        AgeSubj((length(AgeSubj)+1):(length(AgeSubj)+con),1) = Age_PD(i);
        R=[R;B];
    end
    
    clearvars Z
end

T = table(Subject,Group,GroupName,Order,Connections,R,AgeSubj);
writetable(T,'/EXTDATAVOL/stripeik/PD_rsfmri/PD_PH_17March.csv')