%% Code to perform the preprocessing steps 
% This code uses the SPM toolbox. 
% Created by: Eva Blondiaux
% Created at : Laboratory of Cognitive Neuroscience
% Created on : 22.06.2018
%% Pre-processing steps:
clear all;clc;
addpath('C:\Users\eblondia\Downloads\spm12\spm12\');
%% To fill with the path of the folder containing the subjects' Nifti data:
path_subjects='D:\BIDS_Blondiaux_PH_fMRI\';
%%
Subjects = spm_select('List',path_subjects);
Subjects_folders = setdiff(find_file(path_subjects,''),fullfile(path_subjects,cellstr(Subjects)));

%% MR parameters:
TR=2.5;
slices=43;
%% Intialise spm:
spm('Defaults','fMRI');
spm_jobman('initcfg');
for i=1:length(Subjects_folders)
    path_subject=[Subjects_folders{i},filesep];
    [~,Subjectname,~] = fileparts(Subjects_folders{i});
    fprintf('Processing subject %s ..\n',Subjectname);
    niftiFunc_outputdir=fullfile(path_subject,'ses-01',filesep,'func',filesep);
    niftiAnat_outputdir=fullfile(path_subject,'ses-01',filesep,'anat',filesep);
    %% PRE-PROCESSING:
    PreprocessingSteps(niftiFunc_outputdir,niftiAnat_outputdir,Subjectname,slices,TR);
    %% Calculates the framewise displacement:
    Calculate_FD(niftiFunc_outputdir,Subjectname,0.5);
    
end

