%% Code to decompress the nifti files in order to be supported by SPM 
% Adapted by Eva Blondiaux 
%% 

%%
BIDS_path = 'D:\BIDS_TestMATLAB\'; 
matlabbatch{1}.cfg_basicio.file_dir.file_ops.cfg_gunzip_files.files = cellstr(spm_select('FPListRec',BIDS_path,'.gz$'));
%%
matlabbatch{1}.cfg_basicio.file_dir.file_ops.cfg_gunzip_files.outdir = {''};
matlabbatch{1}.cfg_basicio.file_dir.file_ops.cfg_gunzip_files.keep = true;
spm_jobman('run',matlabbatch);