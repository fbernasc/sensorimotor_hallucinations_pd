function plot_movement(rp_file,Subjectname,scans,runID)
%% Created by Eva Blondiaux
% Function to plot the FD for a run
%% Inputs: 
% rp_file: filename of the text file containing the 6 rotations movements
% Subjectname : subject's ID
% Scans: Number of scans of the sequence tested. 
% Run: ID of the sequence
%% 
fprintf('Ploting movement parameters of subject %s\n',Subjectname);
mvt=load(rp_file);
time_point=1:scans;
figure
subplot(2,1,1)
plot(time_point,mvt(:,1),'b');
hold on
plot(time_point,mvt(:,2),'r');
hold on
plot(time_point,mvt(:,3),'g');
title(sprintf('%s:Movement parameters %s',Subjectname,runID));
subplot(2,1,2)
plot(time_point,mvt(:,4),'b');
hold on
plot(time_point,mvt(:,5),'r');
hold on
plot(time_point,mvt(:,6),'g');
hold off
title(sprintf('%s:Rotation parameters %s',Subjectname,runID))
path = fileparts(rp_file); 
file2=[path,runID,'_motion_parameters_',Subjectname];
save(file2);
end