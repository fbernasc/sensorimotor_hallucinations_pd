% Calculates the framewise displacement using the
% bramila_framewiseDisplacement function beased on Power paper.
% Adapted and created by: Eva Blondiaux
%% Inputs: 
% niftiFunc_outputdir : Path where the functional data is stored
% Subjectname : ID of the subject
% threshold: threshold to be used for the FD 
%% 
function Calculate_FD(niftiFunc_outputdir,Subjectname,threshold)
RPfiles = spm_select('FPList',niftiFunc_outputdir,'^rp_');
RPfiles = cellstr(RPfiles); 
run={'MotorLoc','Run1','Run2','TouchLoc'};
scans = {160,320,320,160};
%% Calculates the FD for the runs:
for i=1:length(RPfiles)
    filename=RPfiles{i};
    plot_movement(filename,Subjectname,scans{i},run{i});
    cfg.motionparam=num2str(filename);
    cfg.prepro_suite = 'spm';
    cfg.radius =50;
    [fwd,rms]=bramila_framewiseDisplacement(cfg);
    outofthreshold=length(find(fwd>threshold));
    if sum(outofthreshold)~=0
        fprintf('%s Movement above threshold: %i points above in %s\n',Subjectname,outofthreshold,run{i});
    end
    % Calculate the mean FD:
    FD{i}=mean(fwd);
end

%% Write values into a log file
FD=FD';
log_file=[niftiFunc_outputdir,'Log_file_folders_Sub_',Subjectname,'_FD.txt'];
fileID=fopen(log_file,'w');
nrows=size(FD,1);
Runs={'MotorLoc';'Run1';'Run2';'Touchloc'};
fprintf(fileID,'Runs Mvt_FD\n');
FD=cell2mat(FD);
for row=1:nrows
    fprintf(fileID,'%s %s \n',Runs{row,:},num2str(roundn(FD(row),-3)));
end
%% Check if one value is higher than 0.5:
if ismember(1,(FD>threshold))
    fprintf(fileID,'One run has to much movement: %s',num2str(cell2mat(Runs(cell2mat(FD)>0.5))));
else
    fprintf(fileID,'All runs are fine!!');
end
fclose(fileID);

end