function PreprocessingSteps(niftiFunc_outputdir,niftiAnat_outputdir,Subjectname,slices,TR)
%% Preprocessing steps
% The inpout parameters are: 
% niftiFunc_outputdir = subject's folder containing the functional Nifti files (in 4D).
% niftiAnat_outputdir = subject's folder containing the anat file
% Subjectname: name of the subject
% slices: number of slices acquiered
% TR: repetition time of the MR sequence
% Created by : Eva Blondiaux 
% EPFL, Geneva, Laboratory of Cognitive Neuroscience
%% 
% Selects the Nifti files in the folder: 
% For the functional files: 
RobotStimRun1 = spm_select('ExtFPList', niftiFunc_outputdir,'run-1_bold.nii$',1:320);
RobotStimRun2 = spm_select('ExtFPList', niftiFunc_outputdir,'run-2_bold.nii$',1:320);
MotorLoc = spm_select('ExtFPList', niftiFunc_outputdir,'.*\Motor.*\.nii$',1:160);
TouchLoc = spm_select('ExtFPList', niftiFunc_outputdir,'.*\Touch.*\.nii$',1:160);
anat = spm_select('FPList', niftiAnat_outputdir,'.nii$');

%% Starts the preprocessing: 
tmp_files=which('TPM.nii');
n={',1';',2';',3';',4';',5';',6'};
tmp_files=strcat(tmp_files,n);
clear matlabbatch
fprintf('Subject %s\n',Subjectname)
%% SLICE TIMING:
matlabbatch{1}.spm.temporal.st.scans ={cellstr(RobotStimRun1),cellstr(RobotStimRun2),cellstr(MotorLoc),cellstr(TouchLoc)};
matlabbatch{1}.spm.temporal.st.nslices = slices;
matlabbatch{1}.spm.temporal.st.tr = TR;
matlabbatch{1}.spm.temporal.st.ta = TR-(TR/slices);
matlabbatch{1}.spm.temporal.st.so = [1:2:slices 2:2:slices];
matlabbatch{1}.spm.temporal.st.refslice = 1;
matlabbatch{1}.spm.temporal.st.prefix = 'a';
spm_jobman('run',matlabbatch);
clear matlabbatch
%% REALIGNMENT:
RobotStimRun1 = spm_select('ExtFPList', niftiFunc_outputdir,'^a.*\_run-1_bold.nii$',1:320);
RobotStimRun2 = spm_select('ExtFPList', niftiFunc_outputdir,'^a.*\_run-2_bold.nii$',1:320);
MotorLoc = spm_select('ExtFPList', niftiFunc_outputdir,'^a.*\Motor.*\.nii$',1:160);
TouchLoc = spm_select('ExtFPList', niftiFunc_outputdir,'^a.*\Touch.*\.nii$',1:160);
fprintf('Subject %s',Subjectname)
matlabbatch{1}.spm.spatial.realign.estwrite.data{1} = cellstr(RobotStimRun1);
matlabbatch{1}.spm.spatial.realign.estwrite.data{2} = cellstr(RobotStimRun2);
matlabbatch{1}.spm.spatial.realign.estwrite.data{3} =cellstr(MotorLoc);
matlabbatch{1}.spm.spatial.realign.estwrite.data{4} = cellstr(TouchLoc);
matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.quality = 0.9;
matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.sep = 4;
matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.fwhm = 5;
matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.rtm = 1;
matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.interp = 2;
matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.wrap = [0 0 0];
matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.weight = '';
matlabbatch{1}.spm.spatial.realign.estwrite.roptions.which = [2 1];
matlabbatch{1}.spm.spatial.realign.estwrite.roptions.interp = 4;
matlabbatch{1}.spm.spatial.realign.estwrite.roptions.wrap = [0 0 0];
matlabbatch{1}.spm.spatial.realign.estwrite.roptions.mask = 1;
matlabbatch{1}.spm.spatial.realign.estwrite.roptions.prefix = 'r';
%% COREGISTER:
spm_jobman('run',matlabbatch);
clear matlabbatch
mean_image = spm_select('FPList', niftiFunc_outputdir,'^m.*\.nii$');
matlabbatch{1}.spm.spatial.coreg.estimate.ref = cellstr(mean_image);
matlabbatch{1}.spm.spatial.coreg.estimate.source = cellstr(anat);
matlabbatch{1}.spm.spatial.coreg.estimate.other = {''};
matlabbatch{1}.spm.spatial.coreg.estimate.eoptions.cost_fun = 'nmi';
matlabbatch{1}.spm.spatial.coreg.estimate.eoptions.sep = [4 2];
matlabbatch{1}.spm.spatial.coreg.estimate.eoptions.tol = [0.02 0.02 0.02 0.001 0.001 0.001 0.01 0.01 0.01 0.001 0.001 0.001];
matlabbatch{1}.spm.spatial.coreg.estimate.eoptions.fwhm = [7 7];
%% SEGMENTATION:
matlabbatch{2}.spm.spatial.preproc.channel.vols = cellstr(anat);
matlabbatch{2}.spm.spatial.preproc.channel.biasreg = 0.001;
matlabbatch{2}.spm.spatial.preproc.channel.biasfwhm = 60;
matlabbatch{2}.spm.spatial.preproc.channel.write = [0 0];
matlabbatch{2}.spm.spatial.preproc.tissue(1).tpm = cellstr(tmp_files{1});
matlabbatch{2}.spm.spatial.preproc.tissue(1).ngaus = 1;
matlabbatch{2}.spm.spatial.preproc.tissue(1).native = [1 0];
matlabbatch{2}.spm.spatial.preproc.tissue(1).warped = [0 0];
matlabbatch{2}.spm.spatial.preproc.tissue(2).tpm = cellstr(tmp_files{2});
matlabbatch{2}.spm.spatial.preproc.tissue(2).ngaus = 1;
matlabbatch{2}.spm.spatial.preproc.tissue(2).native = [1 0];
matlabbatch{2}.spm.spatial.preproc.tissue(2).warped = [0 0];
matlabbatch{2}.spm.spatial.preproc.tissue(3).tpm = cellstr(tmp_files{3});
matlabbatch{2}.spm.spatial.preproc.tissue(3).ngaus = 2;
matlabbatch{2}.spm.spatial.preproc.tissue(3).native = [1 0];
matlabbatch{2}.spm.spatial.preproc.tissue(3).warped = [0 0];
matlabbatch{2}.spm.spatial.preproc.tissue(4).tpm = cellstr(tmp_files{4});
matlabbatch{2}.spm.spatial.preproc.tissue(4).ngaus = 3;
matlabbatch{2}.spm.spatial.preproc.tissue(4).native = [1 0];
matlabbatch{2}.spm.spatial.preproc.tissue(4).warped = [0 0];
matlabbatch{2}.spm.spatial.preproc.tissue(5).tpm = cellstr(tmp_files{5});
matlabbatch{2}.spm.spatial.preproc.tissue(5).ngaus = 4;
matlabbatch{2}.spm.spatial.preproc.tissue(5).native = [1 0];
matlabbatch{2}.spm.spatial.preproc.tissue(5).warped = [0 0];
matlabbatch{2}.spm.spatial.preproc.tissue(6).tpm = cellstr(tmp_files{6});
matlabbatch{2}.spm.spatial.preproc.tissue(6).ngaus = 2;
matlabbatch{2}.spm.spatial.preproc.tissue(6).native = [0 0];
matlabbatch{2}.spm.spatial.preproc.tissue(6).warped = [0 0];
matlabbatch{2}.spm.spatial.preproc.warp.mrf = 1;
matlabbatch{2}.spm.spatial.preproc.warp.cleanup = 1;
matlabbatch{2}.spm.spatial.preproc.warp.reg = [0 0.001 0.5 0.05 0.2];
matlabbatch{2}.spm.spatial.preproc.warp.affreg = 'mni';
matlabbatch{2}.spm.spatial.preproc.warp.fwhm = 0;
matlabbatch{2}.spm.spatial.preproc.warp.samp = 3;
matlabbatch{2}.spm.spatial.preproc.warp.write = [1 1];
spm_jobman('run',matlabbatch);
clear matlabbatch
%% Select the new files:
RobotStimRun1 = spm_select('FPList', niftiFunc_outputdir,'^r.*\_run-1_bold.nii$');
RobotStimRun2 = spm_select('FPList', niftiFunc_outputdir,'^r.*\_run-2_bold.nii$');
MotorLoc = spm_select('FPList', niftiFunc_outputdir,'^r.*\Motor.*\.nii$');
TouchLoc = spm_select('FPList', niftiFunc_outputdir,'^r.*\Touch.*\.nii$');
def=spm_select('FPlist',niftiAnat_outputdir,'^y_.*\.nii$');
%% RUN1
fprintf('Subject %s\n',Subjectname)
matlabbatch{1}.spm.util.defs.comp{1}.def = cellstr(def);
matlabbatch{1}.spm.util.defs.out{1}.pull.fnames = cellstr(RobotStimRun1);
matlabbatch{1}.spm.util.defs.out{1}.pull.savedir.saveusr =cellstr(niftiFunc_outputdir);
matlabbatch{1}.spm.util.defs.out{1}.pull.interp = 4;
matlabbatch{1}.spm.util.defs.out{1}.pull.mask = 1;
matlabbatch{1}.spm.util.defs.out{1}.pull.fwhm = [0 0 0];
%% RUN2
matlabbatch{2}.spm.util.defs.comp{1}.def = cellstr(def);
matlabbatch{2}.spm.util.defs.out{1}.pull.fnames = cellstr(RobotStimRun2);
matlabbatch{2}.spm.util.defs.out{1}.pull.savedir.saveusr = cellstr(niftiFunc_outputdir);
matlabbatch{2}.spm.util.defs.out{1}.pull.interp = 4;
matlabbatch{2}.spm.util.defs.out{1}.pull.mask = 1;
matlabbatch{2}.spm.util.defs.out{1}.pull.fwhm = [0 0 0];
%% MOVEMENT LOC
matlabbatch{3}.spm.util.defs.comp{1}.def = cellstr(def);
matlabbatch{3}.spm.util.defs.out{1}.pull.fnames = cellstr(MotorLoc);
matlabbatch{3}.spm.util.defs.out{1}.pull.savedir.saveusr =cellstr(niftiFunc_outputdir);
matlabbatch{3}.spm.util.defs.out{1}.pull.interp = 4;
matlabbatch{3}.spm.util.defs.out{1}.pull.mask = 1;
matlabbatch{3}.spm.util.defs.out{1}.pull.fwhm = [0 0 0];
%% TOUCH LOC
matlabbatch{4}.spm.util.defs.comp{1}.def =cellstr(def);
matlabbatch{4}.spm.util.defs.out{1}.pull.fnames = cellstr(TouchLoc);
matlabbatch{4}.spm.util.defs.out{1}.pull.savedir.saveusr =cellstr(niftiFunc_outputdir);
matlabbatch{4}.spm.util.defs.out{1}.pull.interp = 4;
matlabbatch{4}.spm.util.defs.out{1}.pull.mask = 1;
matlabbatch{4}.spm.util.defs.out{1}.pull.fwhm = [0 0 0];
spm_jobman('run',matlabbatch);
clear matlabbatch
%% SMOOTH DATA:----------------------------------------------------------------------------
RobotStimRun1 = spm_select('FPList',niftiFunc_outputdir,'^w.*\_run-1_bold.nii$');
RobotStimRun2 = spm_select('FPList', niftiFunc_outputdir,'^w.*\_run-2_bold.nii$');
MotorLoc = spm_select('FPList', niftiFunc_outputdir,'^w.*\Motor.*\.nii$');
TouchLoc = spm_select('FPList', niftiFunc_outputdir,'^w.*\Touch.*\.nii$');
def=spm_select('FPlist',niftiAnat_outputdir,'^y_.*\.nii$');
wrt=spm_select('FPlist',niftiAnat_outputdir,'^(c1|c2|c3|s).*\.nii$');
%% Run1
fprintf('Subject %s',Subjectname)
matlabbatch{1}.spm.spatial.smooth.data = cellstr(RobotStimRun1);
matlabbatch{1}.spm.spatial.smooth.fwhm = [6 6 6];
matlabbatch{1}.spm.spatial.smooth.dtype = 0;
matlabbatch{1}.spm.spatial.smooth.im = 0;
matlabbatch{1}.spm.spatial.smooth.prefix = 's';
%% Run2
matlabbatch{2}.spm.spatial.smooth.data = cellstr(RobotStimRun2);
matlabbatch{2}.spm.spatial.smooth.fwhm = [6 6 6];
matlabbatch{2}.spm.spatial.smooth.dtype = 0;
matlabbatch{2}.spm.spatial.smooth.im = 0;
matlabbatch{2}.spm.spatial.smooth.prefix = 's';
%% Mvt Loc
matlabbatch{3}.spm.spatial.smooth.data = cellstr(MotorLoc);
matlabbatch{3}.spm.spatial.smooth.fwhm = [6 6 6];
matlabbatch{3}.spm.spatial.smooth.dtype = 0;
matlabbatch{3}.spm.spatial.smooth.im = 0;
matlabbatch{3}.spm.spatial.smooth.prefix = 's';
%% Touch loc
matlabbatch{4}.spm.spatial.smooth.data = cellstr(TouchLoc);
matlabbatch{4}.spm.spatial.smooth.fwhm = [6 6 6];
matlabbatch{4}.spm.spatial.smooth.dtype = 0;
matlabbatch{4}.spm.spatial.smooth.im = 0;
matlabbatch{4}.spm.spatial.smooth.prefix = 's';
%% Segmentation steps + normalisation of anatomical images:
matlabbatch{5}.spm.util.imcalc.input = cellstr(wrt);
matlabbatch{5}.spm.util.imcalc.output = 'anat_skull_strip.nii';
matlabbatch{5}.spm.util.imcalc.outdir = cellstr(niftiAnat_outputdir);
matlabbatch{5}.spm.util.imcalc.expression = '(i1+i2+i3 > 0.9).*i4';
matlabbatch{5}.spm.util.imcalc.var = struct('name', {}, 'value', {});
matlabbatch{5}.spm.util.imcalc.options.dmtx = 0;
matlabbatch{5}.spm.util.imcalc.options.mask = 0;
matlabbatch{5}.spm.util.imcalc.options.interp = 1;
matlabbatch{5}.spm.util.imcalc.options.dtype = 4;
spm_jobman('run',matlabbatch);
clear matlabbatch
t1=spm_select('FPlist',niftiAnat_outputdir,'^s.*\.nii$');
skull_t1=spm_select('FPlist',niftiAnat_outputdir,'^anat_skull.*\.nii$');
spm_check_registration(t1,skull_t1);
fprintf('Subject %s',Subjectname)
fullstripped=spm_select('FPlist',niftiAnat_outputdir,'^anat_.*\.nii$');
matlabbatch{1}.spm.util.defs.comp{1}.def = cellstr(def);
matlabbatch{1}.spm.util.defs.out{1}.pull.fnames(1) = cellstr(fullstripped);
matlabbatch{1}.spm.util.defs.out{1}.pull.savedir.saveusr = cellstr(niftiAnat_outputdir);
matlabbatch{1}.spm.util.defs.out{1}.pull.interp = 4;
matlabbatch{1}.spm.util.defs.out{1}.pull.mask = 1;
matlabbatch{1}.spm.util.defs.out{1}.pull.fwhm = [0 0 0];
matlabbatch{2}.spm.util.defs.comp{1}.def = cellstr(def);
matlabbatch{2}.spm.util.defs.out{1}.pull.fnames = cellstr(anat);
matlabbatch{2}.spm.util.defs.out{1}.pull.savedir.saveusr = cellstr(niftiAnat_outputdir);
matlabbatch{2}.spm.util.defs.out{1}.pull.interp = 4;
matlabbatch{2}.spm.util.defs.out{1}.pull.mask = 1;
matlabbatch{2}.spm.util.defs.out{1}.pull.fwhm = [0 0 0];
%% Run the batches:
spm_jobman('run',matlabbatch);
end