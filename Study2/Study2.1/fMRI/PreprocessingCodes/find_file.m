function filename_path=find_file(path,tag,extension)
if iscell(path)
    path=path{1};
end
temp=dir(path);
temp={temp.name}';
if ~isempty(tag)
    filenames=temp(~cellfun('isempty',regexp(temp,tag)));
    if length(filenames)>1
        for i=1:length(filenames)
            [p,name,ext]=fileparts(filenames{i});
            if strcmp(ext,extension)
                filename=fullfile(path,filesep,filenames{i});
            end
        end
    else
        filename=fullfile(path,filesep,filenames{1});
    end
    if isdir(filename)
        filename_path=fullfile(filename,filesep);
    else
        filename_path=filename;
    end
else
    temp=setdiff(temp,{'.';'..'});
    filenames=temp;
    filename_path=fullfile(path,filesep,filenames);
end

end