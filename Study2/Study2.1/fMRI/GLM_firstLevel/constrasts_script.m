function constrasts_script(Subjectname,GLM_dir)
%% Code for the contrast analyses: 
% Subjectname: Subject name 
% GLM_dir: GLM directory to put the results
% niftiFunc_outputdir: folder containing the nifti files
%%
% Created by : Eva Blondiaux
% Created at : Laboratory of Cognitive Neuroscience, EPFL
%% 
clear matlabbatch
fprintf('Subject %s: Contrasts ..\n',Subjectname)
SPMfile = spm_select('FPlist',GLM_dir,'SPM.mat');
matlabbatch{1}.spm.stats.con.spmmat = cellstr(SPMfile);
matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'asynch>rest';
matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 zeros(1,11) 1];
matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'synch>rest';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [0 0 1 zeros(1,11) 1];
matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{3}.tcon.name = 'mvt>rest';
matlabbatch{1}.spm.stats.con.consess{3}.tcon.weights = [zeros(1,24),1];
matlabbatch{1}.spm.stats.con.consess{3}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{4}.tcon.name = 'touch>rest';
matlabbatch{1}.spm.stats.con.consess{4}.tcon.weights = [zeros(1,34), 1];
matlabbatch{1}.spm.stats.con.consess{4}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{5}.tcon.name = 'asynch>mvt';
matlabbatch{1}.spm.stats.con.consess{5}.tcon.weights = [1, zeros(1,11),1,zeros(1,11),-1];
matlabbatch{1}.spm.stats.con.consess{5}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{6}.tcon.name = 'synch>mvt';
matlabbatch{1}.spm.stats.con.consess{6}.tcon.weights = [0,0, 1,zeros(1,11),1,zeros(1,9),-1];
matlabbatch{1}.spm.stats.con.consess{6}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{7}.tcon.name = 'asynch>touch';
matlabbatch{1}.spm.stats.con.consess{7}.tcon.weights = [1, zeros(1,11),1,zeros(1,21),-1];
matlabbatch{1}.spm.stats.con.consess{7}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{8}.tcon.name = 'synch>touch';
matlabbatch{1}.spm.stats.con.consess{8}.tcon.weights = [0,0, 1,zeros(1,11),1,zeros(1,19),-1];
matlabbatch{1}.spm.stats.con.consess{8}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{9}.tcon.name = 'mvt+touch>rest';
matlabbatch{1}.spm.stats.con.consess{9}.tcon.weights = [zeros(1,24),1,zeros(1,9),1];
matlabbatch{1}.spm.stats.con.consess{9}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{10}.tcon.name = 'asynch<mvt';
matlabbatch{1}.spm.stats.con.consess{10}.tcon.weights = [-1, zeros(1,11),-1,zeros(1,11),1];
matlabbatch{1}.spm.stats.con.consess{10}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{11}.tcon.name = 'synch<mvt';
matlabbatch{1}.spm.stats.con.consess{11}.tcon.weights = [0,0, -1,zeros(1,11),-1,zeros(1,10),1];
matlabbatch{1}.spm.stats.con.consess{11}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{12}.tcon.name = 'asynch<touch';
matlabbatch{1}.spm.stats.con.consess{12}.tcon.weights = [-1, zeros(1,11),-1,zeros(1,21),1];
matlabbatch{1}.spm.stats.con.consess{12}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{13}.tcon.name = 'synch<touch';
matlabbatch{1}.spm.stats.con.consess{13}.tcon.weights = [0,0, -1,zeros(1,11),-1,zeros(1,20),1];
matlabbatch{1}.spm.stats.con.consess{13}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{14}.tcon.name = 'mvt+touch<rest';
matlabbatch{1}.spm.stats.con.consess{14}.tcon.weights = [zeros(1,24),-1,zeros(1,9),-1];
matlabbatch{1}.spm.stats.con.consess{14}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{15}.tcon.name = 'asynch>synch';
matlabbatch{1}.spm.stats.con.consess{15}.tcon.weights = [1 0 -1 zeros(1,9) 1 0 -1];
matlabbatch{1}.spm.stats.con.consess{15}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{16}.tcon.name = 'asynch<synch';
matlabbatch{1}.spm.stats.con.consess{16}.tcon.weights = [-1 0 1 zeros(1,9) -1 0 1];
matlabbatch{1}.spm.stats.con.consess{16}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.delete = 1;
filename=[GLM_dir,filesep,'Bactch_contrast_',Subjectname];
save(filename,'matlabbatch');
spm_jobman('run',matlabbatch);
end