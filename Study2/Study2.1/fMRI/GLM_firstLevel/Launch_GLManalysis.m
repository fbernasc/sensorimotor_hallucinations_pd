%% Code to perform the GLM analysis first level in healthy subjects
% This code uses the SPM toolbox.
% Created by: Eva Blondiaux
% Created at : Laboratory of Cognitive Neuroscience, EPFL
% Created on : 22.06.2018
%--------------------------------------------------------------------------
clear all;clc;
addpath('C:\Users\eblondia\Downloads\spm12\spm12\');
%% To fill with the path of the folder containing the subjects' Nifti data:
path_subjects='D:\BIDS_TestMATLAB\';
%%
Subjects = spm_select('List',path_subjects);
Subjects_folders = setdiff(find_file(path_subjects,''),fullfile(path_subjects,cellstr(Subjects)));
%% MR parameters:
TR=2.5;
slices=43;
spm('Defaults','fMRI');
spm_jobman('initcfg');
for i=1:length(Subjects_folders)
    path_subject=[Subjects_folders{i},filesep];
    [~,Subjectname,~] = fileparts(Subjects_folders{i});
    fprintf('Subject %s is being analysed..\n',Subjectname);
    %% Creates a result folder in each subject folder if not already present to store the first level analyses:
    GLM_dir=[path_subject,'1stLevel',filesep];
    if ~exist(GLM_dir, 'dir')
        mkdir(GLM_dir);
    end
    niftiFunc_outputdir=fullfile(path_subject,'ses-01',filesep,'func',filesep);
    niftiAnat_outputdir=fullfile(path_subject,'ses-01',filesep,'anat',filesep);
    %% Run the GLM analysis:
    GLManalysis_script(Subjectname,GLM_dir,TR,slices,niftiFunc_outputdir,niftiAnat_outputdir);
    %% Run the first level contrasts: 
    constrasts_script(Subjectname,GLM_dir);
end