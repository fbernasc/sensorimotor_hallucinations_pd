%% Convert the tsv files into files that SPM can read
% eventfilename: filename of the event.tsv
% fileconverted : return the name of the mat file containing the multicondition file
%%
% Created by : Eva Blondiaux
% Created at : Laboratory of Cognitive Neuroscience, EPFL
%%
clear all;clc;
addpath('C:\Users\eblondia\Downloads\spm12\spm12\');
%% To fill with the path of the folder containing the subjects' Nifti data:
path_subjects='D:\BIDS_TestMATLAB\';
%%
tsveventFiles = spm_select('FPListRec',path_subjects,'_events.tsv$');
tsveventFiles = cellstr(tsveventFiles);
for f = 1: length(tsveventFiles)
    event = spm_load(tsveventFiles{f});
    [pathfile,id] = fileparts(tsveventFiles{f});
    % Define the pmod structure:
    pmod = struct('name',{''},'param',{},'poly',{});
    % Identify the trial names:
    trial_names = unique(event.trial_type);
    trial_name = setdiff(trial_names,{'A1';'A2'});
    
    % According to the trial type will save different variables:
    if strcmp(trial_name,'Mvt')
        names = trial_names';
        durations={unique(event.duration(strcmp(event.trial_type,'Mvt'))),...
            unique(event.duration(strcmp(event.trial_type,'A1'))),...
            unique(event.duration(strcmp(event.trial_type,'A2')))};
        onsets = {event.onset(strcmp(event.trial_type,'Mvt')),...
            event.onset(strcmp(event.trial_type,'A1')),...
            event.onset(strcmp(event.trial_type,'A2'))};
        pmod(1).name{1}='mvt';
        pmod(1).param{1}=event.robotmvt_pmod(strcmp(event.trial_type,'Mvt'));
        pmod(1).poly{1}=1;
    elseif strcmp(trial_name,'Touch')
        names= trial_names';
        durations={unique(event.duration(strcmp(event.trial_type,'Touch'))),...
            unique(event.duration(strcmp(event.trial_type,'A1'))),...
            unique(event.duration(strcmp(event.trial_type,'A2')))};
        onsets = {event.onset(strcmp(event.trial_type,'Touch')),...
            event.onset(strcmp(event.trial_type,'A1')),...
            event.onset(strcmp(event.trial_type,'A2'))};
    else
        names= trial_names';
        durations={unique(event.duration(strcmp(event.trial_type,'Asynchronous'))),...
            unique(event.duration(strcmp(event.trial_type,'Synchronous'))),...
            unique(event.duration(strcmp(event.trial_type,'A1'))),...
            unique(event.duration(strcmp(event.trial_type,'A2')))};
        onsets = {event.onset(strcmp(event.trial_type,'Asynchronous')),...
            event.onset(strcmp(event.trial_type,'Synchronous')),...
            event.onset(strcmp(event.trial_type,'A1')),...
            event.onset(strcmp(event.trial_type,'A2'))};
        pmod(1).name{1}='mvt_asynch';
        pmod(2).name{1}='mvt_synch';
        pmod(1).param{1}=event.robotmvt_pmod(strcmp(event.trial_type,'Asynchronous'));
        pmod(2).param{1}=event.robotmvt_pmod(strcmp(event.trial_type,'Synchronous'));
        pmod(1).poly{1}=1;
        pmod(2).poly{1}=1;
        
    end
    
    %% Creates the mat file:-----------------------------------------------------------------------------------------
    fileconverted=[pathfile,filesep,'MultiREG_',id,'.mat'];
    if strcmp(trial_name,'Touch')
        save(fileconverted,'durations','names','onsets')
    else
        save(fileconverted,'durations','names','onsets','pmod')
    end
end
