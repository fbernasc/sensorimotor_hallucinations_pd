function GLManalysis_script(Subjectname,GLM_dir,TR,slices,niftiFunc_outputdir,niftiAnat_outputdir)
%% first level GLM analysis 
% Subjectname: Subject name 
% GLM_dir: GLM directory to put the results
% TR: repetition time
% slices: number of slices
% niftiFunc_outputdir: folder containing the nifti files
% niftiAnat_outputdir: folder containing the anat file
%%
% Created by : Eva Blondiaux
% Created at : Laboratory of Cognitive Neuroscience, EPFL
% Created on : 22.06.2018
%%

% Selects the Nifti files in the folder:  
RobotStimRun1 = spm_select('ExtFPList', niftiFunc_outputdir,'^sw.*\_run-1_bold.nii$',1:320);
RobotStimRun2 = spm_select('ExtFPList', niftiFunc_outputdir,'^sw.*\_run-2_bold.nii$',1:320);
MotorLoc = spm_select('ExtFPList', niftiFunc_outputdir,'^sw.*\.*\Motor.*\.nii$',1:160);
TouchLoc = spm_select('ExtFPList', niftiFunc_outputdir,'^sw.*\.*\Touch.*\.nii$',1:160);

% Selects the movement files: 
RP_RobotStimRun1 = spm_select('FPList', niftiFunc_outputdir,'^rp.*\_run-1_bold.txt$');
RP_RobotStimRun2 = spm_select('FPList', niftiFunc_outputdir,'^rp.*\_run-2_bold.txt$');
RP_MotorLoc = spm_select('FPList', niftiFunc_outputdir,'^rp.*\Motor.*\.txt$');
RP_TouchLoc = spm_select('FPList', niftiFunc_outputdir,'^rp.*\Touch.*\.txt$');

% Select the multireg files creates with convertsBIDSevent.m 
multiREG_RobotStimRun1 = spm_select('FPList', niftiFunc_outputdir,'_run-1_events.mat$');
multiREG_RobotStimRun2 = spm_select('FPList', niftiFunc_outputdir,'_run-2_events.mat$');
multiREG_MotorLoc = spm_select('FPList', niftiFunc_outputdir,'Motor.*\.*\_events.*\.mat$');
multiREG_TouchLoc = spm_select('FPList', niftiFunc_outputdir,'Touch.*\.*\_events.*\.mat$');

% Frequency:
freq=300;
freq1=100;

%% Select the anat file: 
anat=spm_select('FPlist',niftiAnat_outputdir,'^wanat_skull.*\.nii$');
clear matlabbatch
fprintf('Subject %s: Running 1st Level GLM..\n',Subjectname)
%% Run 1 & 2:
matlabbatch{1}.spm.stats.fmri_spec.dir = cellstr(GLM_dir);
matlabbatch{1}.spm.stats.fmri_spec.timing.units = 'secs';
matlabbatch{1}.spm.stats.fmri_spec.timing.RT = TR;
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t = slices;% number of slices 
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0 = 1; % slice of reference for alignment
matlabbatch{1}.spm.stats.fmri_spec.sess(1).scans = cellstr(RobotStimRun1);
matlabbatch{1}.spm.stats.fmri_spec.sess(1).multi = cellstr(multiREG_RobotStimRun1);
matlabbatch{1}.spm.stats.fmri_spec.sess(1).regress = struct('name', {}, 'val', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(1).multi_reg = cellstr(RP_RobotStimRun1);
matlabbatch{1}.spm.stats.fmri_spec.sess(1).hpf = freq;
matlabbatch{1}.spm.stats.fmri_spec.sess(2).scans = cellstr(RobotStimRun2);
matlabbatch{1}.spm.stats.fmri_spec.sess(2).multi = cellstr(multiREG_RobotStimRun2);
matlabbatch{1}.spm.stats.fmri_spec.sess(2).regress = struct('name', {}, 'val', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(2).multi_reg = cellstr(RP_RobotStimRun2);
matlabbatch{1}.spm.stats.fmri_spec.sess(2).hpf = freq;
matlabbatch{1}.spm.stats.fmri_spec.sess(3).scans = cellstr(MotorLoc);
matlabbatch{1}.spm.stats.fmri_spec.sess(3).multi = cellstr(multiREG_MotorLoc);
matlabbatch{1}.spm.stats.fmri_spec.sess(3).regress = struct('name', {}, 'val', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(3).multi_reg = cellstr(RP_MotorLoc);
matlabbatch{1}.spm.stats.fmri_spec.sess(3).hpf = freq1;
matlabbatch{1}.spm.stats.fmri_spec.sess(4).scans = cellstr(TouchLoc);
matlabbatch{1}.spm.stats.fmri_spec.sess(4).multi = cellstr(multiREG_TouchLoc);
matlabbatch{1}.spm.stats.fmri_spec.sess(4).regress = struct('name', {}, 'val', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(4).multi_reg = cellstr(RP_TouchLoc);
matlabbatch{1}.spm.stats.fmri_spec.sess(4).hpf = freq1;
matlabbatch{1}.spm.stats.fmri_spec.fact = struct('name', {}, 'levels', {});
matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [0 0];
matlabbatch{1}.spm.stats.fmri_spec.volt = 1;
matlabbatch{1}.spm.stats.fmri_spec.global = 'None';
matlabbatch{1}.spm.stats.fmri_spec.mthresh = 0.8;
matlabbatch{1}.spm.stats.fmri_spec.mask = {anat};
matlabbatch{1}.spm.stats.fmri_spec.cvi = 'AR(1)';
matlabbatch{2}.spm.stats.fmri_est.spmmat = cfg_dep('fMRI model specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{2}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{2}.spm.stats.fmri_est.method.Classical = 1;
filename=[GLM_dir,filesep,'Bactch_preproc_REG_COND_',Subjectname];
save(filename,'matlabbatch');
spm_jobman('run',matlabbatch);
%%
end