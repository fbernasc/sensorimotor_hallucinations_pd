%% Lesion network analysis:
% Eva Blondiaux
% LNCO, EPFL
%%
% This code computes the connectivity map of each lesion in every healthy
% subject. 
%%
clear all; clc; close all; 
%% To fill:
path_subjects='H:\Lesion_analysis\1_Data\';
result='H:\Lesion_analysis\Results\';
mkdir(result);
folders=find_file(path_subjects,'');
TR=1.4;
ROIpath = 'H:\Lesion_analysis\LesionFolder\'; 
ROIs   = find_file(ROIpath,'');
ROIs=sort_nat(cellstr(ROIs));

%% define batch properties
clear BATCH;
BATCH.filename = [result,'allGroup_',num2str(length(folders))];

%% SETUP
BATCH.Setup.isnew=1;
BATCH.Setup.RT = TR;
BATCH.Setup.nsubjects = length(folders);
BATCH.Setup.acquisitiontype = 1; % continous (default value)
BATCH.Setup.analyses = 2; %  seed to voxel
BATCH.Setup.voxelmask = 1; %  Explicit mask (brainmask.nii)
BATCH.Setup.voxelresolution = 3; % functional space used
BATCH.Setup.outputfiles = [0,0,1,1,1]; % write nii volumes for r-maps, p-maps and FDR-p-maps

% specify targeted ROIs
BATCH.Setup.rois.names = cellstr(ROIs);
for h=1:size(ROIs,1)
    BATCH.Setup.rois.files{h}{1} = fullfile(ROIs(h,:));
end
BATCH.Setup.rois.dimensions = repmat({1},1,length(cellstr(ROIs)));%takes the mean times series for each ROIs

for i = 1:length(folders)
    % Path specific to subject: 
    Nifti_outputdir = [folders{i},filesep];
    anat_folder=[Nifti_outputdir,'Nifti',filesep,'Anat'];
    rest_folder=[Nifti_outputdir,'Nifti',filesep,'Rest'];
    t1_file{i} =spm_select('FPList', anat_folder,'^wsub.*\.nii$');
    c1_file{i} = spm_select('FPList',anat_folder,'^wc1');
    c2_file{i} = spm_select('FPList',anat_folder,'^wc2');
    c3_file{i} = spm_select('FPList',anat_folder,'^wc3');
    % specificy functional data per subject {nsub}{nsess}
    BATCH.Setup.functionals{i}{1} = spm_select('FPList', rest_folder,'^sw.*\.nii$');
    % specificy structural data per subject
    BATCH.Setup.structurals{i} = t1_file{i};
    BATCH.Setup.masks.Grey.files{i} = c1_file{i};
    BATCH.Setup.masks.Grey.dimensions=1;
    BATCH.Setup.masks.White.files{i} = c2_file{i};
    BATCH.Setup.masks.White.dimensions=1;
    BATCH.Setup.masks.CSF.files{i} = c3_file{i};
    BATCH.Setup.masks.CSF.dimensions=1;
    
    % specify conditions per subject {ncond}{nsub}{nsess}
    BATCH.Setup.conditions.names = {'rest'};
    BATCH.Setup.conditions.onsets{1}{i}{1} = [0];
    BATCH.Setup.conditions.durations{1}{i}{1} = [inf];
    BATCH.Setup.covariates.names = {'motion'};
    BATCH.Setup.covariates.files{1}{i}{1} = spm_select('FPList',rest_folder,'^rp.*\.txt$');% mvt
    BATCH.Setup.done = 1; % run the SETUP step
end

%% PREPROCESSING

BATCH.Preprocessing.filter = [0.008, 0.09]; % set the passband filter limits
BATCH.Preprocessing.confounds.names = {'White Matter', 'CSF', 'motion'};
BATCH.Preprocessing.confounds.dimensions = {'1','1','6'};
BATCH.Preprocessing.confounds.deriv = {'1','1','2'};
BATCH.Preprocessing.done = 1; % run the PREPROCESSING step

%% FIRST-LEVEL ANALYSIS
BATCH.Analysis.analysis_number = 1;
BATCH.Analysis.type = 3; % do ROI to ROI and seed to voxel analysis
BATCH.Analysis.measure = 1; % measure bivariate correlation
BATCH.Analysis.weight = 1; % use hrf as weight
BATCH.Analysis.sources.names = cellstr(ROIs);
BATCH.Analysis.sources.dimensions = repmat({1},1,length(cellstr(ROIs)));
BATCH.Analysis.sources.deriv = repmat({0},1,length(cellstr(ROIs)));
BATCH.Analysis.done = 1; % run the ANALYSIS step

%% run the BATCH
conn_batch(BATCH);

