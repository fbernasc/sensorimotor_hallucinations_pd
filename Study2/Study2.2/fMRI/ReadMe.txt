Eva Blondiaux
04.05.2020

Lesion network mapping analysis: 

1. Apply LesionNetworkMapping.m once all the lesions are in MNI space and that
database of healthy subjects is ready to use (pre-processed with a folder 
Nifti containing both Rest where the functional is in 4D and Anat with the 
normalised structural of the subject). 
2. Apply Create_ContrastFile.m that will create the average the connectivity of all subjects 
per lesion 
3. Then those maps are thresholded and binarised in order to overlap them.

