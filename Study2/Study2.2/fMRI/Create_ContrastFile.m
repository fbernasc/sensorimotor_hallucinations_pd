%% Creates the contrast files automatically:
% Eva Blondiaux
% 11.02.2019 LNCO, Geneve
%% This function will create the average map of all subjects per lesion, then for each lesion the map will have to be threshold and binarise and then 
% all maps (1 per lesion) can be overlap.
%%
clear all;clc;close all;
conn_file='H:\Lesion_analysis\Results\allGroup_126.mat';
load(conn_file); 
NbROIs=length(CONN_x.Setup.rois.names')-3;
ROIsName=CONN_x.Setup.rois.names(4:end-1);
%% Creates the contrasts:
for i = 1 : NbROIs
fprintf('ROI %i/%i\n',i,NbROIs);
BATCH.Results.between_subjects.effect_names={'AllSubjects'};
BATCH.Results.between_subjects.contrast=1;
BATCH.Results.between_conditions.effect_names={'rest'};
BATCH.Results.between_conditions.contrast=1;
BATCH.Results.between_sources.effect_names=ROIsName(i);
BATCH.Results.between_sources.contrast=1;
BATCH.Results.done = 1; 
conn_batch(BATCH);

end
fprintf('Done\n')